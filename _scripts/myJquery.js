// JavaScript Document
//hover green: rgba(0,55,0,1)
//heading brown:  rgba(79,45,6,1)
$(document).ready(function() {
	    $('.content').hide();
	    $('table').hide();
		//show/hide content 
		//change h2 to whatever encloses the heading
		$('h2.icon').click(function() {
			//wipes down the content
			if ($(this).next().is(':hidden')) {
				$(this).next().slideDown('5000', 'linear');
				//$(this).css('color', 'rgba(0,55,0,1)');
			//wipes up the content
			} else if ($(this).next().is(':visible')) {
				$(this).next().slideUp('5000', 'linear');
				//$(this).css('color', 'rgba(79,45,6,1)');
			};
		}); 
		//show/hide menu section
		$('.menuSection').click(function() {
			//wipe down menu section		
			if ($(this).next().is(':hidden')) {
				$(this).next().slideDown('5000', 'linear');
			//wipes up menu section
			} else if ($(this).next().is(':visible')) {
				$(this).next().slideUp('5000', 'linear');
			};
		});
		//contact form validation
		$('#cmdSubmit').click(function() {
			//if any of the form fields are blank, then return error message and stop link follow-through
			//else then form is good to go, no need to see error message
			if ($('#frmName').val() == '' || $('#frmEmail').val() == '' || $('#frmMessage').val() == '') {
				$('#errorMsg').html("<p><img src='_assets/icons/errorMsg.gif'>Please fill out all fields.<img src='_assets/icons/errorMsg.gif'></p>");
				return false;	
			} else {
				$('#errorMsg').text('');
				return true;	
			}
		});
});