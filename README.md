# Sandwich Shoppe #

This project served as a vehicle for me to learn to write my own media queries and understand jQuery animations.

## On Load -- Contracted View ##
![Screen Shot 2015-12-05 at 12.43.14 PM.png](https://bitbucket.org/repo/eM5MnM/images/2712612550-Screen%20Shot%202015-12-05%20at%2012.43.14%20PM.png)

## Expanded View ##
![screencapture-file-Users-AngieSiuPC-Desktop-Web-20Projects-The-20Sandwich-20Shoppe-index-html-1449337867525.jpg](https://bitbucket.org/repo/eM5MnM/images/2959344851-screencapture-file-Users-AngieSiuPC-Desktop-Web-20Projects-The-20Sandwich-20Shoppe-index-html-1449337867525.jpg)